﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class HealthManagement : MonoBehaviour
{

    public static float maxHp = 100f;
    public float regen = 10f;
    public static float currentHp;
    public Vector3 hpScale;
    public Camera MainCamera;
    private float cccMin = 0;
    private float cccMax = 1;
    public float cccc;

    private ColorCorrectionCurves colorCorrection;
    private MotionBlur M_blur;

    void Start()
    {
        colorCorrection = MainCamera.GetComponent<ColorCorrectionCurves>();
        M_blur = MainCamera.GetComponent<MotionBlur>();
        currentHp = maxHp;
        hpScale = Vector3.one;
        colorCorrection.enabled = true;
        M_blur.enabled = false;
    }

    void Update()
    {
        //HpMonitoring();  //hp effects
        HpRegeneration(regen);
        hpScale.x = currentHp / maxHp;
        this.transform.localScale = hpScale;
        /*    if (currentHp <= 0)
            {
                UnityEditor.EditorApplication.isPlaying = false;  // playeris mirsta, game over
            }*/
    }

    public static void TakeDamage(float dmg)
    {
        currentHp -= dmg;
        currentHp = Mathf.Clamp(currentHp, 0, int.MaxValue);
    }

    public static void OnLeverUp()
    {
        maxHp += 20;
        currentHp = maxHp;
    }

    private void HpMonitoring()
    {
        colorCorrection.saturation = currentHp * cccMax / maxHp;
        cccc = colorCorrection.saturation;
        colorCorrection.saturation = Mathf.Clamp(colorCorrection.saturation, cccMin, cccMax);

        if (currentHp <= maxHp * 0.2)
        {
            M_blur.enabled = true;
        }
        else
        {
            M_blur.enabled = false;
        }
    }

    public void HpRegeneration(float regen)
    {
        if (currentHp < maxHp)
            currentHp += regen * Time.deltaTime;
    }
}
