﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChannelNova : MonoBehaviour {

    public GameObject Nova;
    public ParticleSystem OuterNova;
    public GameObject CompletedNova;
    public GameObject CompletedNovaForThrow;
    Rigidbody rb;
    public bool NovaChanneled;
    public float ProjectileSpeed = 1000f;
    public float TimeChanneling = 0;
	// Use this for initialization
	void Start () {
        Nova.SetActive(false);
        NovaChanneled = false;
        CompletedNova.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey("f"))
        {
            Nova.SetActive(true);
            TimeChanneling += Time.deltaTime;
            if (TimeChanneling >= OuterNova.startLifetime)
                NovaChanneled = true;
            else
                NovaChanneled = false;

            if (NovaChanneled)
            {
                Nova.SetActive(false);
                CompletedNova.SetActive(true);
            }
        }
        if (Input.GetKeyUp("f"))
        {
            if (NovaChanneled)
                throwNova();
            TimeChanneling = 0;
            Nova.SetActive(false);
            CompletedNova.SetActive(false);
        }


    }

    void throwNova()
    {
    //    GetComponent<AudioSource>().PlayOneShot(Fireball_Throw);
        var nova = Instantiate(CompletedNovaForThrow, transform.position, Quaternion.identity);
        rb = nova.GetComponent<Rigidbody>();
        var dir = Vector3.forward;
        rb.AddForce(transform.TransformDirection(dir) * ProjectileSpeed);
    }
}
