﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperienceManagement : MonoBehaviour {

    //exp management
    public static int Experience = 0;
    public int ExpToNextLvl = 25;
    public int Level = 1;
    private GameObject PlayerHealthBar;
    private HealthManagement hp;

    //for exp bar
    public float maxExp;
    public float currentExp;
    public Vector3 expScale;

    void Start()
    {
        expScale = Vector3.one;
    }

    void Update()
    {
        OnLevelUp();

        currentExp = Experience;
        maxExp = ExpToNextLvl;
        expScale.x = currentExp / maxExp;
        transform.localScale = expScale;
    }

    public static void AddExp(float exp)
    {
        Experience += (int)exp;
        Debug.Log(Experience);
    }

    void OnLevelUp()
    {
        if (Experience >= ExpToNextLvl)
        {
            GetComponent<AudioSource>().Play();
            Level++;
            Experience -= ExpToNextLvl;
            ExpToNextLvl += 15;
            HealthManagement.OnLeverUp();
        }
    }
}
