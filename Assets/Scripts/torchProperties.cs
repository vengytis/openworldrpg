﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;
using UnityEngine;

public class torchProperties : MonoBehaviour {

    public ParticleSystem Flame;
    public GameObject Target;
    public float Range = 10f;
    public float Distance;
    private Color differentColor;
    private Color nColor;
    private float nLifeTime;
    public ColorCorrectionCurves ccc;
    public GameObject cameraObj;
    public Camera mc;
    public Material mid;
    public Material bottom;

    void Start () {
        Flame = GetComponent<ParticleSystem>();
        differentColor = new Color(0 / 255, 255 / 255, 0 / 255, 255 / 255);
        Target = GameObject.FindGameObjectWithTag("Player");
        cameraObj = GameObject.FindGameObjectWithTag("MainCamera");
        mc = cameraObj.GetComponent<Camera>();
        nColor = Flame.startColor;
        nLifeTime = Flame.startLifetime;
        ccc = cameraObj.GetComponent<ColorCorrectionCurves>();
    }
	
	void Update () {
        float metallic = Mathf.PingPong(Time.time, 1.0f);
        Distance = Vector3.Distance(transform.position, Target.transform.position);

        if (Distance <= Range)
        {
            Flame.startColor = differentColor;
            Flame.startLifetime = 3f;
            mid.SetFloat("_Metallic", metallic);
            bottom.SetFloat("_Metallic", metallic);
            ccc.saturation = (Distance - 1.8f) / Range * 5;
        }
        else
        {
            Flame.startColor = nColor;
            Flame.startLifetime = nLifeTime;
        //    ccc.saturation = 1;
        }
	}
}
