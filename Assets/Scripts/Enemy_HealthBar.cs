﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_HealthBar : MonoBehaviour {

    public float maxHp;
    public float currentHp;
    public Vector3 hpScale;
    public Enemy_Wolf WolfHP;

    void Start()
    {
        maxHp = WolfHP.GetWolfHP();
        hpScale = Vector3.one;
    }

    void Update()
    {
        currentHp = WolfHP.GetWolfHP();
        hpScale.x = currentHp / maxHp;
        this.transform.localScale = hpScale;
    }
}
