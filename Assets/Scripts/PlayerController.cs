﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    public float walkSpeed = 6;
    public float runSpeed = 10;
    CharacterController charControl;
    public float turnSmoothTime = 0.2f;
    float turnSmoothVelocity;
    Rigidbody rb;
    public float speedSmoothTime = 0.1f;
    float speedSmoothVelocity;
    public float currentSpeed;
    public float vertical = 0;
    public float horizontal = 0;
    Animator animator;
    Camera camera;

    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        camera = GetComponentInChildren<Camera>();
        charControl = GetComponent<CharacterController>();
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {

        //  Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        //   Vector2 inputDir = input.normalized;

        //if (inputDir != Vector2.zero)
        //{
        //    float targetRotation = Mathf.Atan2(inputDir.x, inputDir.y) * Mathf.Rad2Deg;
        //    transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, turnSmoothTime);
        //}



        //transform.Translate(transform.forward * currentSpeed * Time.deltaTime, Space.World);

        /*   float verticalAxis = Input.GetAxis("Vertical");
           float horizontalAxis = Input.GetAxis("Horizontal");

           verticalAxis *= Time.deltaTime;
           horizontalAxis *= Time.deltaTime;

            bool running = Input.GetKey(KeyCode.LeftShift);
             float targetSpeed = ((running) ? runSpeed : walkSpeed);
             currentSpeed = Mathf.SmoothDamp(currentSpeed, targetSpeed, ref speedSmoothVelocity, speedSmoothTime);*/

        /* float translation = Input.GetAxis("Vertical") * runSpeed;
         float straffe = Input.GetAxis("Horizontal") * runSpeed;
         float velocity = 0;
         translation *= Time.deltaTime;
         straffe *= Time.deltaTime;

         if (Input.GetKey("Vertical"))
             velocity += 0;
         Vector2 inputDir = camera.transform.forward;

         transform.Translate(camera.transform.forward * Time.deltaTime, Space.World);

         //      if (Input.GetKeyDown("escape"))
         //          Cursor.lockState = CursorLockMode.None;

         //  transform.Translate(horizontalAxis * currentSpeed, 0, verticalAxis * currentSpeed, Space.World);

         //   float animationSpeedPercent = ((running) ? 1 : .5f) * currentSpeed;
         float animationSpeedPercent = runSpeed / runSpeed;
         animator.SetFloat("speedPercent", animationSpeedPercent, speedSmoothTime, Time.deltaTime);*/

        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");


        Vector3 moveDirSide = transform.right * horiz * walkSpeed;
        Vector3 moveDirForward = transform.forward * vert * walkSpeed;

        //  charControl.SimpleMove(moveDirSide);
        //  charControl.SimpleMove(moveDirForward);

        if (Input.GetKey(KeyCode.W))
        {
            vertical += walkSpeed;
            rb.velocity += new Vector3(horizontal, 0, vertical);
        //    rb.AddForce(new Vector3(horizontal, 0, vertical));
        }

        if (Input.GetButton("Horizontal"))
        {
            horizontal += walkSpeed;
            rb.velocity += new Vector3(horizontal, 0, vertical);
        //    rb.AddForce(new Vector3(horizontal, 0, vertical));
        }

        
    }
}