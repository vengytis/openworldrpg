﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireStream : MonoBehaviour {

    public float DamagePerSec = 20f;
    public GameObject Fire;
    public float ToTarget;
    public float DistanceToTarget;
    public float maxRange = 12;
    public Camera MainCamera;
    private Underwater UnderWaterScript;
    private void Start()
    {
        Fire.SetActive(false);
        UnderWaterScript = MainCamera.GetComponent<Underwater>();
    }
    void Update () {
        if (Input.GetKey("f"))
        {
            if (!UnderWaterScript.inWater)
            {
                Fire.SetActive(true);
                attack();
            }
            else
                Fire.SetActive(false);
        }
        else
            Fire.SetActive(false);
    }

    public void attack()
    {
        RaycastHit Hit;
        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out Hit))
        {
            ToTarget = Hit.distance;
            DistanceToTarget = ToTarget;

                if (DistanceToTarget < maxRange)
                {
                    Hit.transform.SendMessage("ApplyDamage", DamagePerSec * Time.deltaTime, SendMessageOptions.DontRequireReceiver);
                }
        }
    }
}
