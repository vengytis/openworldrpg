﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeSystem : MonoBehaviour {
    public static float DistanceToTarget;
    public float ToTarget;
    public float Range = 20f;
    public Transform TheSystem;

    int Damage = 15;
	
	void Update () {
        attack();
        if (Input.GetButtonDown("Fire1"))
        {
            GetComponent<Animation>().Play();
            GetComponent<AudioSource>().Play();
        }
    }

    public void attack()
    {
        RaycastHit Hit;
        if (Physics.Raycast(TheSystem.transform.position, TheSystem.transform.TransformDirection(Vector3.forward), out Hit))
        {
            ToTarget = Hit.distance;
            DistanceToTarget = ToTarget;
            if (Input.GetButtonDown("Fire1"))
            {

                if (DistanceToTarget < Range)
                {
                    Hit.transform.SendMessage("ApplyDamage", Damage, SendMessageOptions.DontRequireReceiver);
                }
            }
        }
    }
    private void OnTriggerEnter(Collider col)
    {
        Debug.Log("colliding with " + col.name);
        col.transform.SendMessage("ApplyDamage", Damage, SendMessageOptions.DontRequireReceiver);
    }
}
