﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowAnimatedTexture : MonoBehaviour {

    public GameObject texture;
    public GameObject player;
    float Distance;
    float maxDistance = 10;
    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        Distance = Vector3.Distance(transform.position, player.transform.position);
        float z;
        z = texture.transform.position.z + Distance / maxDistance / 10;
        if (Distance < maxDistance)
        {
            texture.transform.position = new Vector3 (texture.transform.position.x, texture.transform.position.y, z);
        }
    }
}
