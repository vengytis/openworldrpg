﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lightning_line_renderer : MonoBehaviour {

    public GameObject target;
    LineRenderer LR;
    public float arcLength = .0f;
    public float arcVariation = 2.0f;
    float inaccuracy = 1.0f;

    // Use this for initialization
    void Start () {
        LR = GetComponent<LineRenderer>();
	}


    void Update()
    {
        var lastPoint = transform.position;
        var i = 1;
       // arcLength = Random.Range(1, 3);
      //  arcVariation = Random.Range(1, 10);
        LR.SetPosition(0, transform.position);
        while (Vector3.Distance(target.transform.position, lastPoint) > .5 && Vector3.Distance(target.transform.position, lastPoint) < 20.0)
        {
          //  LR.SetVertexCount(i + 1);
            LR.numPositions = i + 1;
            var fwd = target.transform.position - lastPoint;
            fwd.Normalize();
            fwd = Randomize(fwd, inaccuracy);
            fwd *= Random.Range(arcLength * arcVariation, arcLength);
            fwd += lastPoint;
            LR.SetPosition(i, fwd);
            i++;
            lastPoint = fwd;
        }
    }

    Vector3 Randomize(Vector3 v3, float inaccuracy2)
    {
        v3 += new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f)) * inaccuracy2;
        v3.Normalize();
        return v3;
    }
}
