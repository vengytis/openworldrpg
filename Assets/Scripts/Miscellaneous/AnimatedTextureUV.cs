﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedTextureUV : MonoBehaviour
{
    public int _uvTieX = 6;
    public int _uvTieY = 5;
    public int _fps = 5;

    private Vector2 _size;
    private Renderer _myRenderer;
    private int _lastIndex = -1;
    public Renderer rend;
    void Start()
    {
        rend = GetComponent<Renderer>();
        _size = new Vector2(1.0f / _uvTieX, 1.0f / _uvTieY);
        _myRenderer = rend;
        if (_myRenderer == null)
        {
            enabled = false;
            Debug.Log(false);
        }
    }
    // Update is called once per frame
    void Update()
    {
        // Calculate index
        int index = (int)(Time.timeSinceLevelLoad * _fps) % (_uvTieX * _uvTieY);
        if (index != _lastIndex)
        {
            // split into horizontal and vertical index
            int uIndex = index % _uvTieX;
            int vIndex = index / _uvTieY;

            // build offset
            // v coordinate is the bottom of the image in opengl so we need to invert.
            Vector2 offset = new Vector2(uIndex * _size.x, 1.0f - _size.y - vIndex * _size.y);

            _myRenderer.material.SetTextureOffset("_MainTex", offset);
            _myRenderer.material.SetTextureScale("_MainTex", _size);

            _lastIndex = index;
        }
    }
}
