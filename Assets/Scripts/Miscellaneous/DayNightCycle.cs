﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightCycle : MonoBehaviour {

    public float cycleMins = 10;
    public float cycleCalc;
	
	void Update () {
        cycleCalc = 0.1f / cycleMins * -1;

        transform.Rotate(0, 0, cycleCalc, Space.World);
	}
}
