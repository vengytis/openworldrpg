﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMouseHover : MonoBehaviour
{
    public GameObject pointer_left;
    public GameObject pointer_right;
    // Use this for initialization
    private void OnMouseOver()
    {
        pointer_left.SetActive(true);
        pointer_right.SetActive(true);
        Debug.Log("entered");
    }
}
