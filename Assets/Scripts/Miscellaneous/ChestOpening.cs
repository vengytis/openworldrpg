﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestOpening : MonoBehaviour {

    public bool Opened = false;
    public float Range = 3f;
    public Transform Player;
    
	// Use this for initialization
	void Start () {
        Opened = false;
    }
	
	// Update is called once per frame
	void Update () {
        float distance = Vector3.Distance(Player.transform.position, transform.position);

        if (distance < Range)
        {
            GetComponentInChildren<Animation>().Play("Chest_Open");
            Opened = true;
        }
        Opened = false;
	}
}
