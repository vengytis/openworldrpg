﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBump : MonoBehaviour {

    public Renderer rend;
    public GameObject player;
    float Distance;
    float maxDistance = 10;
    // Use this for initialization
    void Start () {
        rend = GetComponent<Renderer>();
        player = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update () {
        Distance = Vector3.Distance(transform.position, player.transform.position);
        if (Distance < maxDistance)
        {
            rend.material.SetFloat("_Shininess", Distance/maxDistance);
        }
    }
}
