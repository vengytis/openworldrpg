﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleportation : MonoBehaviour {

    public GameObject player;
    public float Distance;
    public Transform otherPortal;

	void Start () {

    }
	
	void Update () {
        Distance = Vector3.Distance(transform.position, player.transform.position);
        if (Distance <= 1.2)
        {
            player.transform.position = new Vector3(otherPortal.position.x, otherPortal.position.y + 4, otherPortal.position.z);
        }
	}
}
