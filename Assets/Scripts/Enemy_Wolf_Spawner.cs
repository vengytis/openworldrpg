﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Wolf_Spawner : MonoBehaviour {

    GameMonitor enemy_monitor;
    PoolManager po;
    public GameObject prefab;
    public static GameObject[] Enemies_array;
    Enemy_Wolf wolf;
    // Use this for initialization
    void Start () {
        Enemies_array = GameObject.FindGameObjectsWithTag("Enemy");
        Debug.Log("Enemy wolves count: " + Enemies_array.Length);
        PoolManager.instance.CreatePool(prefab, 1);
    }
	
	// Update is called once per frame
	void Update () {
        Enemies_array = GameObject.FindGameObjectsWithTag("Enemy");
        Debug.Log("Enemy wolves count: " + Enemies_array.Length);
        RespawnWolf();
	}

    void RespawnWolf()
    {
        if (Enemies_array.Length == 0)
        {
            PoolManager.instance.ReuseObject(prefab, new Vector3(transform.position.x, transform.position.y + 1, transform.position.z), Quaternion.identity);
        }
    }
}
