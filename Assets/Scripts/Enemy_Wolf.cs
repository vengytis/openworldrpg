﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Wolf : MonoBehaviour {

    public GameObject Target;
    public float Distance;
    public float Range = 60f;
    public float Speed = 2f;
    public float Health = 100f;
    public float Damage = 5f;
    public float AttackRange = 3f;
    public GameObject bloodEffect;
    public Material fur;
    private int Exp = 10;
    //private bool tookDamage;

    void Start () {
        Target = GameObject.FindGameObjectWithTag("Player");
	}
	
	void Update () {
        Distance = Vector3.Distance(transform.position, Target.transform.position);
        if ((Distance <= Range && Distance > AttackRange))
        {
            Chase();
            fur.SetColor("_Color", Color.yellow);
            fur.SetFloat("_Metallic", 0.75f);
        }
        else if (Distance < AttackRange)
        {
            Attack();
            fur.SetColor("_Color", Color.red);
            fur.SetFloat("_Metallic", 1.0f);
        }
        else
        {
            fur.SetColor("_Color", Color.white);
            fur.SetFloat("_Metallic", 0.5f);
        }
        if (Health <= 0)
        {
            ExperienceManagement.AddExp(Exp);
            GameMonitor.RemoveWolf(gameObject);
            Destroy(this.gameObject);
        }
    }

    void Chase()
    {
        var lookAtPlayer = Quaternion.LookRotation(Target.transform.position - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookAtPlayer, Time.deltaTime * 5);
        GetComponent<Animation>().Play("Wolf_run");
        var movementVector = Vector3.forward * Speed * Time.deltaTime;
        transform.Translate(movementVector);

    }

    public float GetWolfHP()
    {
        return Health;
    }

    public void Attack() //enemy calls this
    {
        HealthManagement.TakeDamage(Damage * Time.deltaTime);
    }

    public void ApplyDamage(float dmg) //player calls this method by attacking
    {
        Health -= dmg;
        Instantiate(bloodEffect, new Vector3(transform.position.x, transform.position.y + 1.5f, transform.position.z), Quaternion.identity);
    }
}
