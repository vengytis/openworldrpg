﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMonitor : MonoBehaviour {

    public static List<GameObject> Enemies = new List<GameObject>();
    public static GameObject[] Enemies_array;
        // Use this for initialization
    void Start () {
        Enemies_array = GameObject.FindGameObjectsWithTag("Enemy");
    }

    // Update is called once per frame
    void Update () {
        Enemies_array = GameObject.FindGameObjectsWithTag("Enemy");
    }

    public static void RemoveWolf(GameObject wolf)
    {
        Enemies.Remove(wolf);
    }

    public static int WolvesCount()
    {
        return Enemies_array.Length;
    }
}
