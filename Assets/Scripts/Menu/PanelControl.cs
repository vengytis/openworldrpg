﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelControl : MonoBehaviour
{

    public GameObject Panel;
    public GameObject SpellsPanel;
    public GameObject WeaponsPanel;
    public Camera MainCamera;
    public Transform Player;
    public Transform camTX;
    public float CameraX;
    public float CameraY;
    public float CameraZ;
    void Start()
    {
        Panel.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        camTX = MainCamera.GetComponent<Transform>();
    }

    void Update()
    {

        if (Panel.activeSelf)
        {
            //    MainCamera.transform.rotation = Quaternion.identity;
            //    MainCamera.transform.localRotation = new Quaternion(CameraX, CameraY, CameraZ);
            //  MainCamera.transform.rotation = new Vector3(rotation.x, rotation.y, rotation.z);
            //      MainCamera.transform.
        //    camTX.rotation = Quaternion.Euler(CameraX, CameraY + 180, CameraZ);
         //   camTX.rotation = Quaternion.identity;
            camTX.eulerAngles -= new Vector3(CameraX, CameraY, CameraZ);
        }
        else
        {
            //     rotation = new Vector3(MainCamera.transform.rotation.x, MainCamera.transform.rotation.y, MainCamera.transform.rotation.z);
            CameraX = MainCamera.transform.rotation.x;
            CameraY = MainCamera.transform.rotation.y;
            CameraZ = MainCamera.transform.rotation.z;
        }
        if (Input.GetKeyDown(KeyCode.Tab))
            if (Panel.activeSelf)
            {
                /*        if (Input.GetKeyDown(KeyCode.Escape))
                        {
                            if (SpellsPanel.activeSelf)
                                SpellsPanel.SetActive(false);
                            else
                                disablePanel();
                        }*/
                disablePanel();
            }
            else
            {
                enablePanel();

            }
    }

    void enablePanel()
    {
        Panel.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale = 0;
    }

    void disablePanel()
    {
        Panel.SetActive(false);
        SpellsPanel.SetActive(false);
        WeaponsPanel.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale = 1;
    }
}
