﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapons : MonoBehaviour
{

    public Button StickBtn;
    public Button SwordBtn;
    public GameObject Melee;
    public GameObject Stick;
    public GameObject Sword;

    public int weaponsCount = 0;

    public List<GameObject> weapons = new List<GameObject>();
    void Start()
    {
        weaponsCount = Melee.transform.childCount;
        StickBtn.onClick.AddListener(TurnOnStickOnClick);
        SwordBtn.onClick.AddListener(TurnOnSwordOnClick);
        for (int i = 0; i < weaponsCount; i++)
            weapons.Add(Melee.transform.GetChild(i).gameObject);

        foreach (GameObject wp in weapons)
            wp.SetActive(false);
    }

    void TurnOnStickOnClick()
    {
        Debug.Log("Stick equipped");
        TurnOfOtherWeapons(Stick);
    }

    void TurnOnSwordOnClick()
    {
        Debug.Log("Sword equipped");
        TurnOfOtherWeapons(Sword);
    }

    void TurnOfOtherWeapons(GameObject weapon)
    {
        foreach (GameObject wp in weapons)
            wp.SetActive(false);

        weapon.SetActive(true);
    }
}
