﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Buttons : MonoBehaviour {

    public Button MagicBtn;
    public GameObject SpellsPanel;
    public Button WeaponBtn;
    public GameObject WeaponsPanel;
    public GameObject Magic;
    public GameObject Melee;
    void Start()
    {
        MagicBtn.onClick.AddListener(MagicBtnOnClick);
        SpellsPanel.SetActive(false);
        WeaponBtn.onClick.AddListener(WeaponBtnOnClick);
        WeaponsPanel.SetActive(false);
    }

    void MagicBtnOnClick()
    {
        Debug.Log("Spells menu is now open");
        Magic.SetActive(true);
        SpellsPanel.SetActive(true);
        Melee.SetActive(false);
        WeaponsPanel.SetActive(false);
    }

    void WeaponBtnOnClick()
    {
        Debug.Log("Weapons menu is now open");
        Melee.SetActive(true);
        WeaponsPanel.SetActive(true);
        Magic.SetActive(false);
        SpellsPanel.SetActive(false);
    }
}
