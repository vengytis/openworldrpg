﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spells : MonoBehaviour
{

    public Button FireStreamBtn;
    public Button ColdFireBtn;
    public Button NovaBtn;
    public GameObject Magic;
    public GameObject FireStream;
    public GameObject ColdFire;
    public GameObject Nova;

    public int spellCount = 0;

    public List<GameObject> spells = new List<GameObject>();
    void Start()
    {
        spellCount = Magic.transform.childCount;
        FireStreamBtn.onClick.AddListener(TurnOnFireStreamOnClick);
        ColdFireBtn.onClick.AddListener(TurnOnColdFireOnClick);
        NovaBtn.onClick.AddListener(TurnOnNovaOnClick);
        for (int i = 0; i < spellCount; i++)
            spells.Add(Magic.transform.GetChild(i).gameObject);

        foreach (GameObject sp in spells)
            sp.SetActive(false);
    }

    void TurnOnFireStreamOnClick()
    {
        Debug.Log("Fire Stream turned on");
        TurnOfOtherSpells(FireStream);
    }

    void TurnOnColdFireOnClick()
    {
        Debug.Log("Cold Fire turned on");
        TurnOfOtherSpells(ColdFire);
    }

    void TurnOnNovaOnClick()
    {
        Debug.Log("Nova turned on");
        TurnOfOtherSpells(Nova);
    }

    void TurnOfOtherSpells(GameObject spell)
    {
        foreach (GameObject sp in spells)
            sp.SetActive(false);

        spell.SetActive(true);
    }
}
