﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogMonitoring : MonoBehaviour {


    private Color normalColor;
    private Color underwaterColor;
    public float ColorR = 44f;
    public float ColorG = 103f;
    public float ColorB = 206f;

    void Start () {
        underwaterColor = new Color(ColorR / 255, ColorG / 255, ColorB / 255, 1f);
    }

    void Update () {

    }

    public void SetUnderwaterFog()
    {
        RenderSettings.fogColor = underwaterColor;
        RenderSettings.fogEndDistance = 30f;
        RenderSettings.fog = true;
    }

    public void SetNormalFog()
    {
        normalColor = new Color(.5f, .5f, .5f, .5f);
        RenderSettings.fogColor = normalColor;
        RenderSettings.fogDensity = 0;
        RenderSettings.fog = false;
    }
}
