﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;

public class Underwater : MonoBehaviour
{

    public float waterLevel;
    public Transform water;
    private GameObject blurScript;
    public bool isUnderwater;
    private Color normalColor;
    private Blur blur;
    public ColorCorrectionCurves ccc;
    private Color underwaterColor;
    public FogMonitoring wm;
    public float Distance;
    public float Range;

    public bool inWater;
    

    void Start()
    {
        Range = water.transform.localScale.z;
        blur = GetComponent<Blur>();
        ccc = GetComponent<ColorCorrectionCurves>();
        blur.enabled = false;
        ccc.enabled = true;
        wm.SetNormalFog();
    }

void Update()
    {
        Distance = Vector3.Distance(transform.position, water.transform.position);

        if ((transform.position.y < waterLevel) != isUnderwater)
        {
            isUnderwater = transform.position.y < waterLevel;

            if (isUnderwater && WithinWater())
            {
                inWater = true;
                ccc.enabled = false;
                blur.enabled = true;
                water.GetComponent<AudioSource>().Play();
                wm.SetUnderwaterFog();
            }
            else
            {
                inWater = false;
                blur.enabled = false;
                ccc.enabled = true;
                water.GetComponent<AudioSource>().Stop();
                wm.SetNormalFog();
            }
        }
    }

    bool WithinWater()
    {
        if (Distance <= Range)
            return true;
        return false;
    }
}