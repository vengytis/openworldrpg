﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowFireball : MonoBehaviour
{

    public GameObject StartingFireball;
    public GameObject MidFireball;
    public GameObject Explosion;
    public AudioClip Fireball_Start;
    public AudioClip Fireball_Throw;
    public AudioClip Fireball_Explosion;
    public float ProjectileSpeed = 1000f;
    public Camera MainCamera;
    private Underwater UnderWaterScript;
    private Rigidbody rb;
    private void Start()
    {
        UnderWaterScript = MainCamera.GetComponent<Underwater>();
        StartingFireball.SetActive(false);
    }

    void Update()
    {

        if (Input.GetKey("f"))
        {
            if (!UnderWaterScript.inWater)
            {
                StartingFireball.SetActive(true);
            }
        }
        else
            StartingFireball.SetActive(false);

        if (Input.GetKeyUp("f"))
        {
            if (!UnderWaterScript.inWater)
            {
                throwFireball();
            }
        }
    }

    void throwFireball()
    {
        GetComponent<AudioSource>().PlayOneShot(Fireball_Throw);
        var fireball = Instantiate(MidFireball, transform.position, Quaternion.identity);
        rb = fireball.GetComponent<Rigidbody>();
        var dir = Vector3.forward;
        rb.AddForce(transform.TransformDirection(dir) * ProjectileSpeed);
    }
}
