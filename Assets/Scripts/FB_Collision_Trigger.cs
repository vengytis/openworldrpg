﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FB_Collision_Trigger : MonoBehaviour {

    public float Damage = 40f;
    public float Distance;
    public float MaxRange = 50f;
    public Vector3 startingPos;
    public Vector3 currentPos;

    public void Awake()
    {
        startingPos = transform.position;
    }
    public void Update()
    {
        currentPos = this.transform.position;
        Distance = Vector3.Distance(startingPos, currentPos);

        if (Distance >= MaxRange)
            Destroy(gameObject);
    }

    public void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Colliding");
        collision.transform.SendMessage("ApplyDamage", Damage, SendMessageOptions.DontRequireReceiver);
        Destroy(gameObject);
    }
}
