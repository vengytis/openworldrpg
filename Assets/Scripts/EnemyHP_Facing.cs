﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHP_Facing : MonoBehaviour {

    public GameObject Target;
    // Use this for initialization
    void Start () {
        Target = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update () {
        var lookAtPlayer = Target.transform.position - transform.position;
        lookAtPlayer.y = 0;
        var rotation = Quaternion.LookRotation(lookAtPlayer);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 5);
    }
}
