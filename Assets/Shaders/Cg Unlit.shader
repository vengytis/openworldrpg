﻿Shader "Custom/blender" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
	_MainTex2("Blend Texture", 2D) = ""{}
	_Blending("Blend Value", Float) = 0.5
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
#pragma target 3.0

		sampler2D _MainTex;
	sampler2D _MainTex2;
	float _Blending;

	struct Input {
		float2 uv_MainTex;
	};

	half _Glossiness;
	half _Metallic;
	fixed4 _Color;

	void surf(Input IN, inout SurfaceOutputStandard o) {
		// Albedo comes from a texture tinted by color
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		fixed4 sourceTex = tex2D(_MainTex, IN.uv_MainTex) * (1 - _Blending);
		fixed4 targetTex = tex2D(_MainTex2, IN.uv_MainTex) * _Blending;
		//	fixed4 blendedTex = (sourceTex + targetTex) - 1; //linear burn
		//	fixed4 blendedTex = sourceTex / (1 - targetTex); //color dodge
	//	fixed4 blendedTex = min(sourceTex, targetTex); //Darken
	//	fixed4 blendedTex = (sourceTex>0.5) *  (1-(1-2 * (sourceTex - 0.5)) * (1 - targetTex)) + (sourceTex <= 0.5) * ((2 * sourceTex) * targetTex); //Overlay 1st shader
		fixed4 blendedTex = 0.5 - 2 * (sourceTex - 0.5)*(targetTex - 0.5); // Exclusion 2nd shader
		o.Albedo = blendedTex.rgb;
		// Metallic and smoothness come from slider variables
		o.Metallic = _Metallic;
		o.Smoothness = _Glossiness;
		o.Alpha = c.a;
	}
	ENDCG
	}
		FallBack "Diffuse"
}