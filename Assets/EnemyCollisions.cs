﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCollisions : MonoBehaviour {

    private void OnTriggerEnter(Collider col)
    {
       // Debug.Log("Enemy collision: " + col.gameObject.tag);
        if (col.gameObject.tag == "Enemy")
        {
            Debug.Log("Enemies colliding");
        }
    }
}
